import { Component, OnInit } from '@angular/core';
import { DateService } from '../date.service';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})

/*
This is the primary content-showing component of the project. 
It retrieves the current date in two formats from an injected DateService and displays this information to the user.
*/
export class ClockComponent implements OnInit {

  public numberFormatted: String | null;
  public dayNameFormatted: String | null;

  /* This use of null as a starting value may not be strictly necessary for just formatting dates,
  but it is intended to show how you might handle a situation where the information is retrieved asynchronously. */
  constructor(private readonly dateService: DateService) {
    this.numberFormatted = null;
    this.dayNameFormatted = null;
   }

  ngOnInit(): void {
    this.numberFormatted = this.dateService.numberFormat();
    this.dayNameFormatted = this.dateService.dayNameFormat();
  }
}
