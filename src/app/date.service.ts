import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/*
This is a service class which formats the current date in two different ways, 
allowing this information to be easily displayed in the Clock component.

The user of a service might be slightly over-engineered for this small task,
but it seemed like a good opportunity to separate the concerns of (simple) data processing
and data presentation.
*/
export class DateService { 
  private date: Date;
  
  constructor() {
    this.date = new Date();
  }

  public numberFormat(): String {
    const day = this.date.getDate() >= 10 ? this.date.getDate() : `0${this.date.getDate()}`
    const month = this.date.getMonth() >= 10 ? this.date.getMonth() : `0${this.date.getMonth()}`
    return (`${day} / ${month} / ${this.date.getFullYear()}`)
  }

  public dayNameFormat(): String {
    const month = this.date.toLocaleString('default', {month : 'short'})
    const dayName = this.date.toLocaleString('default', {weekday : 'short'})
    return ((`${this.date.getDate()} ${month} ${this.date.getFullYear()} [${dayName}]`))
  }
}
