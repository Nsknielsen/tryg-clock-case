# TrygClock
This is a simple clock website built in Angular, which shows the current date in two different formats.

## How to use
To view a local version of the project, open the project root folder in a terminal of your choice. First run `npm install`to ensure that the required packages are installed, then run `ng serve`, provided you have the Angular CLI installed. Navigate to `http://localhost:4200/` to see the project. 
If the project is opened in Google Chrome, you can use the developer tools to also see the project in a slightly different mobile-friendly view.

## Dependencies
Angular standard package.
